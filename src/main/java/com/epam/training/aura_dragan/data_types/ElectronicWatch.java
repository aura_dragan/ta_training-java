package com.epam.training.aura_dragan.data_types;

import java.util.Scanner;

public class ElectronicWatch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int seconds = scanner.nextInt();

        int h = seconds/3600;
        int x = seconds - h * 3600;
        int mm = x/60;
        int ss = x - 60 * mm;

        if (h == 24)
            System.out.println(0 + ":" + mm/10 + mm % 10 + ":" + ss/10 + ss % 10);
        else
            System.out.println(h + ":" + mm/10 + mm % 10 + ":" + ss/10 + ss % 10);
    }
}
