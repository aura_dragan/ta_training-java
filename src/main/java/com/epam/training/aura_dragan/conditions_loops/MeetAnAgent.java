package com.epam.training.aura_dragan.conditions_loops;

import java.util.Scanner;

public class MeetAnAgent {
    final static int PASSWORD = 133976; //You can change pass, but don't change the type

    public static void main(String[] args) {
        //put you code here
        Scanner scanner = new Scanner(System.in);
//        while (true){
        int nr = scanner.nextInt();
//            if (nr == -1) {
//                break;
//            }
        if (nr == PASSWORD) {
            System.out.println("Hello, Agent");
        } else {
            System.out.println("Access denied");
        }
//        }
    }
}
