package com.epam.training.aura_dragan.conditions_loops;

import java.util.Scanner;

public class PizzaSplit {
    public static void main(String[] args) {
        //Write a program, reading number of people and number of pieces per pizza and then
        //printing minimum number of pizzas to order to split all the pizzas equally and with no remainder
        Scanner scanner = new Scanner(System.in);
        int p = scanner.nextInt();
        int f = scanner.nextInt();

        for (int i = 1; i < 200; i++) {
            if ((i * p) % f == 0 && p > 0 && f > 0) {
                System.out.println(i * p / f);
                break;
            }
        }

    }
}

