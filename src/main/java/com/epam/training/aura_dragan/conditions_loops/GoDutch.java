package com.epam.training.aura_dragan.conditions_loops;

import java.util.Scanner;

public class GoDutch {
    public static void main(String[] args) {
        //Write code here
        Scanner scanner = new Scanner(System.in);
        int bill = scanner.nextInt();
        int nr = scanner.nextInt();
        int pay = 0;

        if (bill < 0)
            System.out.println("Bill total amount cannot be negative");
        if (nr <= 0)
            System.out.println("Number of friends cannot be negative or zero");

        if (bill >= 0 && nr > 0) {
            pay = (int)(1.1f * bill / nr);
            System.out.println(pay);
        }
    }
}
