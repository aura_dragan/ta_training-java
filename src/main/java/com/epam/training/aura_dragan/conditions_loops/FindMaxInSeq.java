package com.epam.training.aura_dragan.conditions_loops;

import java.util.Scanner;

public class FindMaxInSeq {
    public static int max() {

        // Put your code here
        Scanner scanner = new Scanner(System.in);
        int max = scanner.nextInt();

        while (scanner.hasNext()) {
            int m = scanner.nextInt();

            if (m == 0)
                break;

            if (m > max) {
                max = m;
            }
        }

        return max;
    }

    public static void main(String[] args) {

        System.out.println("Test your code here!\n");

        // Get a result of your code

        System.out.println(max());
    }
}

