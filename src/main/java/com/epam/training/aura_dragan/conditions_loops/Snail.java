package com.epam.training.aura_dragan.conditions_loops;

import java.util.Scanner;

public class Snail {
    public static void main(String[] args) {
        //Write a program that reads a,b and h (line by lyne in this order) and prints
        //the number of days for which the snail reach the top of the tree.
        //a - feet that snail travels up each day, b - feet that slides down each night, h - height of the tree

        Scanner scannerA = new Scanner(System.in);
        String aa = scannerA.nextLine();
        String bb = scannerA.nextLine();
        String hh = scannerA.nextLine();
        int d = 0;

        int a = Integer.parseInt(aa);
        int b = Integer.parseInt(bb);
        int h = Integer.parseInt(hh);

        if (a >= 0 && b >= 0 && h >= 0) {
            if (a > b && h >= b && b != 0) {
                d = (h - b) / (a - b);
                System.out.println(d);
            } else if (h < b || b == 0 || a == 0) {
                System.out.println(1);
            } else if ((a == b && h == 0) || h > b) {
                System.out.println("Impossible");
            }
        }
    }
}

