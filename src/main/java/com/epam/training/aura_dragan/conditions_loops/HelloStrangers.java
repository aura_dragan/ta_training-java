package com.epam.training.aura_dragan.conditions_loops;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HelloStrangers {
    public static void main(String[] args) throws IOException {
        //Write a program, asks for a number - amount of strangers to meet.
        //Then reads stranger names line by line and prints line by line "Hello, ...".
        Scanner scanner = new Scanner(System.in);
        Scanner scan = new Scanner(System.in);
        int no = scanner.nextInt();
        List<String> list = new ArrayList<>();

        if (no == 0) {
            System.out.println("Oh, it looks like there is no one here");
        } else if (no < 0) {
            System.out.println("Seriously? Why so negative?");
        } else {
            int i = 0;
            while (i < no) {
                String name = scan.nextLine();
                if (name != null && !name.isEmpty()) {
                    list.add(name);
                }
                i++;
            }

            for (int j = 0; j < list.size(); j++) {
                System.out.println("Hello, " + list.get(j));
            }
        }
    }
}
