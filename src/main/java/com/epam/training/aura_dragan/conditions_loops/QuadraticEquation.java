package com.epam.training.aura_dragan.conditions_loops;

import java.util.Locale;
import java.util.Scanner;

import static java.lang.Math.sqrt;

public class QuadraticEquation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        double c = scanner.nextDouble();

        double dd = b * b - 4 * a * c;
        double d = Math.sqrt(dd);
//        System.out.println(dd);

        if (a != 0 && dd >=0) {
            if (d != 0){
                double x1 = (-b - d) / (2 * a);
                double x2 = (-b + d) / (2 * a);
                System.out.println(x1 + " " + x2);
            } else {
                double x = -b / (2 * a);
                System.out.println(x);
            }
        } else {
            System.out.println("no roots");
        }
    }
}