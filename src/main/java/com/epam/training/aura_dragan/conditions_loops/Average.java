package com.epam.training.aura_dragan.conditions_loops;

import java.util.Scanner;

public class Average {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // Use Scanner methods to read input
        int n = 0, s = 0, av = 0, i = 0;

        do {
            n = scanner.nextInt();
            if (n == 0) break;
            s += n;
            i++;
            av = s / i;
        } while (i < Integer.MAX_VALUE);

        System.out.println(av);
    }
}
